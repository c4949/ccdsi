package com.zert.java.tutorial.collections.test1;

import com.zert.java.tutorial.collections.test.Animal;
import com.zert.java.tutorial.collections.test.Calculate;

public class Example extends Calculate {

	public static void main(String[] args) {

		Animal animalref = new Animal();
		String result = animalref.eats();
		System.out.println(result);

		Example example = new Example();
		// Example extends Calculate class so the add method which is protected
		example.add(29, 39);

		Calculate calculate = new Calculate();
		// calculate.add(10,20); //The method add is protected so you can invoke only
		// within the package

	}

}
