package com.zert.java.tutorial.collections.test;

import java.util.HashMap;
import java.util.Set;

public class CCDSIHashMapExample {

	public static void main(String[] args) {

		HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
		hashMap.put("Ramya", 80);
		hashMap.put("Ananya", 10);
		hashMap.put("Aadhu", 3);
		hashMap.put("Arun", 41);
		hashMap.put("Santhi", 70);

		Set<String> keys = hashMap.keySet();

		for (String name : keys) {
			System.out.println(name);
			Integer value = hashMap.get(name);
			System.out.println(value);
		}

		Address addressref = new Address();

		addressref.setStreet("unit G74 ,41, Gotha street");
		addressref.setSuburb("FValley");
		addressref.setCity("Brisbane");

		Address addressref1 = new Address();

		addressref1.setStreet("107, lakshmi appts");
		addressref1.setCity("Chennai");
		addressref1.setSuburb("Jafferkhanpet");

		HashMap<String, Address> hashMap1 = new HashMap<String, Address>();
		hashMap1.put("aadhu", addressref);
		hashMap1.put("ananya", addressref);
		hashMap1.put("santhi", addressref1);

		Set<String> nameSet = hashMap1.keySet();
		for (String name : nameSet) {
			System.out.println(name);

			Address personsaddress = hashMap1.get(name);
			String city = personsaddress.getCity();
			String street = personsaddress.getStreet();
			String suburb = personsaddress.getSuburb();
			System.out.println(city);
			System.out.println(street);
			System.out.println(suburb);
		}

	}

}
