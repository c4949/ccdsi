package com.zert.java.tutorial.collections.test;

public class Person {
	
	private int age;
	
	protected String sex;
	
	public Person() {
		System.out.println("Person is initiated");
	}
	
	public Person(int age, String sex) {
		this.age=age;
		this.sex=sex;
	}
	
	public int getAge() {
		return age;
	}
	
	
	public void setAge(int age) {
		this.age = age;
	}
	
	
	public String getSex() {
		return sex;
	}
	
	
	public void setSex(String sex) {
		this.sex = sex;
	}


}
