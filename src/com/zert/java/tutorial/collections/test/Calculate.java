package com.zert.java.tutorial.collections.test;

public class Calculate {

	private float d;//this variable cannot be accessed outside this class. 
	
	 protected int add(int a, int b) {
		 int c = a+b; 
		 return c ; 
		 
	 }
	
	protected int multiply(int a, int b) {
		int c = a *b ; 
		return c ; 
	}
	
	public static void main(String args[]) {
		Calculate calculate  = new Calculate();
		int addedVal = calculate.add(10,20);
		int addedVal1 = calculate.add(100,200);
		int addedVal2 = calculate.add(8,6);
		int addedVal3 = calculate.add(7,9);
		int addedVal4 = calculate.add(13,15);
		int addedVal5 = calculate.add(14,15);
		int addedVal6 = calculate.add(19,20);
		
		calculate.d=3.12f;  
		
		int result = calculate.multiply(50, 60); 
		
		System.out.println(addedVal);
		System.out.println(addedVal1);
		System.out.println(addedVal2);
		System.out.println(addedVal3);
		System.out.println(addedVal4);
		System.out.println(addedVal5);
		System.out.println(addedVal6);
		System.out.println(result);
		
	}
	
	
	
}
