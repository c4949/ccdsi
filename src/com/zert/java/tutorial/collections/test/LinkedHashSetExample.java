package com.zert.java.tutorial.collections.test;

import java.util.Iterator;
import java.util.LinkedHashSet;

/*
    Maintains insertion Order.
    It won�t allow duplicates.
    It allow one NULL values.
 */

public class LinkedHashSetExample {

	public static void main(String[] args) {
		
	

		LinkedHashSet<String> hashSet = new LinkedHashSet<String>();

		hashSet.add("Naresh");
		hashSet.add("Zaheer");
		hashSet.add("Wahab");
		hashSet.add("Sahul");
		hashSet.add("Amir");
		hashSet.add(null);

		System.out.println(hashSet);

		printHashset(hashSet);

		LinkedHashSet<Integer> hashSetForNumbers = new LinkedHashSet<Integer>();

		hashSetForNumbers.add(5);
		hashSetForNumbers.add(120);
		hashSetForNumbers.add(9);
		hashSetForNumbers.add(18);
		hashSetForNumbers.add(7);
		hashSetForNumbers.add(6);
		hashSetForNumbers.add(6);
		hashSetForNumbers.add(null);
		hashSetForNumbers.add(null);
		
		printHashSetForNumbers(hashSetForNumbers);

	}
		
	
	public static void printHashset(LinkedHashSet<String> hashsetref) {
		for (String personName : hashsetref) {
			System.out.println(personName);
		}
		

		Iterator<String> iterator = hashsetref.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
						  		}
	}
		
		public static void printHashSetForNumbers( LinkedHashSet<Integer> hashSetForNumbers) {
		Iterator<Integer> iterator= hashSetForNumbers.iterator(); 
				while (iterator.hasNext()) {
					System.out.println(iterator.next());
				}
	}
}
