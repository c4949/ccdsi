package com.zert.java.tutorial.collections.test;

import java.util.Collections;

public class PracticePrograms {

	public static void main(String[] args) {
		
		
		int e =98;
		String n = String.valueOf(e);
		System.out.println(n);
		
		 String rs = reverseUsingStringBuffer(n);
		 System.out.println(rs);
		
		int biggestNumberIs = findBiggestNumber(-423,-100000,-2); 
		System.out.println(biggestNumberIs);
		
		double dbiggestNumberIs = findBiggestNumber(-0.00001,-0.00002,-0.00003);
		
		
		double smallestNumber = findSmallestNumber(0,0,0); 
		

		int result =  calculateFactorial(8); 
		 System.out.println(result);
		 
		
		 result =  calculateFactorial(7); 
		 System.out.println(result);
		 
		 result = add(5,6); 
		 System.out.println(result);
		 
		 
		 String original = "aadhavan123";
		 String reversedString = reverseUsingStringBuffer(original);
		 System.out.println("The reversed String using String Buffer:" +reversedString);
		 
		String reversedresult =  reverse(original);
			System.out.println(reversedresult);
			if(original.equals(reversedresult)) {
				System.out.println("String is palindrome");
			}else {
				System.out.println("String is not a palindrome");
			}
			
			
			System.out.println(checkIfPalindrome(9));
		
	}
	
	public static int calculateFactorial(int number) {
		int factorial =  1;
		for (int index = number; index>0;index--) {		
			factorial = factorial * index ; 
		}
			
	return factorial;
	}

	
	public static int findBiggestNumber(int a, int b , int c) {
		int biggest =0;
		if (a > b && a > c) {
			biggest = a;
			System.out.println("a is biggest");
		} else if (b > c && b > a) {
			biggest = b;
			System.out.println("b is biggest");
		} else if (c>a && c>b){
			biggest =c;
			System.out.println("c is biggest");
		} else {
			System.out.println("All are same");
		}
		
	return biggest;
	}
	
	
	public static int findSmallestNumber(int a, int b , int c) {
		int smallest = 0;
		if (a < b && a < c) {
			smallest = a;
			System.out.println("a is smallest");
		} else if (b < c && b < a) {
			smallest = b;
			System.out.println("b is smallest");
		} else if (c < a && c < b){
			smallest =c;
			System.out.println("c is smallest");
		} else {
			System.out.println("All are same");
		}
		
	return smallest;
	}
	
	public static double findBiggestNumber(double a, double  b , double c) {
		double biggest =0;
		if (a > b && a > c) {
			biggest = a;
			System.out.println("a is biggest");
		} else if (b > c && b > a) {
			biggest = b;
			System.out.println("b is biggest");
		} else if (c>a && c>b){
			biggest =c;
			System.out.println("c is biggest");
		} else {
			System.out.println("All are same");
		}
		
	return biggest;
	}
	
	
	public static String reverse(String str ) {
						String reverse ="";
		for(int index=str.length()-1;index>=0;index-- ) {
			//System.out.print(str.charAt(index));
			reverse = reverse + str.charAt(index);
			 
		}
		return reverse ; 
	} 
	
	public static String reverseUsingStringBuffer(String str) {
		StringBuffer sbf = new StringBuffer(str);
		 sbf.reverse();
		String reversedString = sbf.toString();
		return reversedString;
	}
	
	public static int add(int a, int b) {
		int c = a+b;
		return c ; 
	}
	
	
	public static String checkIfPalindrome(int n) { 
		String result ="";
		
		int original= n;
		int sum = 0; 
		
		if(n/10 > 0) {
			
			while(n>0) {
				int r = n%10;
				sum = (sum * 10+r);
				n = n/10; 
			}
			
			
			if(sum == original) {
				result = original  + ": is a palindrome";
			} else {
				result = original  + ": is not a palindrome";
			}
		} else {
			result= "Your input is not a valid candidate for palindrome";
		}
		
		
		return result; 
	}
}
