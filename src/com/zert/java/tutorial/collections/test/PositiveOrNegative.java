package com.zert.java.tutorial.collections.test;

public class PositiveOrNegative {

	public static void main(String[] args) {

		String result = findPositiveOrNeg(10);
		System.out.println(result);
	}

	public static String findPositiveOrNeg(int a) {
		String r = "";
		if (a > 0) {
			r = "The number you have entered is a positive number ";
		} else if (a < 0) {
			r = "The number you have entered is a negative";
		} else {
			r = "The number you have entered is Zero";
		}
		return r;

	}

}
