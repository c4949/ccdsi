package com.zert.java.tutorial.collections.test;

public class Customer extends Person {

	private int customerId;

	private String location;

	public Customer() {

	}

	public Customer(int customerId, String location,int age, String sex) {
		super(age,sex);
		this.customerId = customerId;
		this.location = location;
		super.sex="f"; 
		super.getAge(); 
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
