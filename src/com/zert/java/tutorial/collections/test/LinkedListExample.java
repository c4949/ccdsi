package com.zert.java.tutorial.collections.test;

import java.util.Iterator;
import java.util.LinkedList;

/*
 *  It�s Growable array.
    Allow duplicates & NULL Value.
    Maintain Insertion order 
    Insertion and Deletion will be Fast 
    Implements from List and Queue
 */

public class LinkedListExample {

      public static void main(String[] args) {

            LinkedList<Integer> linkedList = new LinkedList<Integer>();
            
          
            linkedList.add(34);

            linkedList.add(22);

            linkedList.add(564);

            linkedList.add(987);

            linkedList.add(null);

            linkedList.add(11);

            linkedList.add(null);

            linkedList.add(123);

            linkedList.add(313);

            linkedList.add(3366);
            

            System.out.println(linkedList);
            
            System.out.println("*************");
            
            for(int index=0;linkedList.size()-index>0;index++ ) {
            	System.out.println(linkedList.get(index));
            }
            
            System.out.println("*************");

            for(Integer ref : linkedList) {
            	System.out.println(ref);
            }
            
            System.out.println("*************");
            
            Iterator<Integer> iter= linkedList.iterator();
           while (iter.hasNext()) {
        	   System.out.println(iter.next());
           }

            LinkedList<String> linkedList1=new LinkedList<String>();

            linkedList1.add("gggg");

            linkedList1.add("wwww");

            linkedList1.add("qqqq");

            linkedList1.add("rrrr");

            linkedList1.add(null);

            linkedList1.add("ffff");

            linkedList1.add("");

            linkedList1.add("rrrr");

            linkedList1.add("");

            System.out.println(linkedList1);

      }

}