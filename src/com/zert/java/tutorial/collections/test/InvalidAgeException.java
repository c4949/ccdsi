package com.zert.java.tutorial.collections.test;

public class InvalidAgeException extends RuntimeException {
	
	public InvalidAgeException(String msg) {
		super(msg);
	}

}
