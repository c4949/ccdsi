package com.zert.java.tutorial.collections.test;

public class NumberComparison {
	
	

	public  void findBiggestOfThreeNumbers(int a , int b , int c) {
		if(compare(a, b, c)) 
		{
			System.out.println("A is largest");
		} else if (compare(b, a, c)) {
			System.out.println("B is largest");
		} else if (compare(c, a, b)) {
			System.out.println("C is largest");
		} else {
			System.out.println("All numbers are equal");
		}
	}

	private boolean compare(int firstNumber, int secondNumber, int thirdNumber) {
		return firstNumber> secondNumber && firstNumber> thirdNumber;
	}
	

}
