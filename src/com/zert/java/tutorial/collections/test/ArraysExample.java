package com.zert.java.tutorial.collections.test;

public class ArraysExample {

	public static void main(String[] args) {
		int[] numbers = new int[10];
		
		numbers[0] = 10;
		numbers[1] = 20;
		numbers[2] = 30;
		numbers[3] = 40;
		numbers[4] = 50;
		
		System.out.println(numbers[0]);
		System.out.println(numbers[1]);
		System.out.println(numbers[2]);
		System.out.println(numbers[3]);
		System.out.println(numbers[4]);
		System.out.println(numbers[5]);
		
		System.out.println("The length of the array is: " +numbers.length);
		
		String[] word = new String[5]; 
		word[0]= "aadhav";
		word[1]= "ani"; 
		word[2]= "kamatchi"; 
		word[3]= "appa"; 
			
		int[] numbersArray = {10,20,30,40,50};
		numbersArray[4] = 60;
		
		
		String[] name = new String[10];
		char[] name1 = new char[10];
		
		name1[0] = 'B';
		
		float[] name2 = new float[10];
		
		boolean[] booleanArray = new boolean[4];
		
		long[] longArray = new long[5];
		
		booleanArray[0]=true;
		booleanArray[1]=false;
		booleanArray[2]=false;
		booleanArray[3]=true;
		System.out.println(booleanArray[3]);
		
		System.out.println("************Below is the Java 5 Enhanced for loop for boolean array**************");
		
		for(boolean val: booleanArray) {
			System.out.println(val);
		}
		
		
		
		Employee[] employeeArray = new Employee[3];
		
		Employee employee1 = new Employee();
		employee1.setEmpId(100);
		employee1.setEmpName("Ramu");
		
		Employee employee2 = new Employee();
		employee2.setEmpId(100);
		employee2.setEmpName("Ravi");
		
		Employee employee3 = new Employee();
		employee3.setEmpId(100);
		employee3.setEmpName("Madan");
		
		employeeArray[0] = employee1;
		employeeArray[1] = employee2;
		employeeArray[2] = employee3;
		
	//	System.out.println("What is in employeeArray[3] :" + employeeArray[3]);
		
		String empName = employeeArray[0].getEmpName();
		
		System.out.println(empName);
		
		System.out.println("**************************");
		
		for(int index=0; index < employeeArray.length; index++ ) {
			Employee employee = employeeArray[index];
			System.out.println(employee.getEmpName());
		}
		
		
		System.out.println("************Below is the Java 5 Enhanced for loop**************");
		
		for(Employee emp: employeeArray) {
			System.out.println(emp.getEmpId());
		}
		
		String s = "5";
		
		System.out.println(Integer.parseInt(s));
		
		Integer b1 = new Integer(5);
		System.out.println(b1.toString());
		
		// Autoboxing
		Integer z = new Integer(5);
		
		// Auto unboxing is placing the object in a primitive
		int a = b1;
		
		System.out.println("**************************");
		
   		int maxValue = Integer.max(3, 5);
   		System.out.println(maxValue);
		
		
		
		

	}

}
