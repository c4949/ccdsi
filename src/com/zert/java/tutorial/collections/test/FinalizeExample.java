package com.zert.java.tutorial.collections.test;

public class FinalizeExample {
	
	public static void main (String args[]) {
		FinalizeExample finalizeExample = new FinalizeExample();
		System.out.println(finalizeExample);
		finalizeExample = null;
		System.gc();
		System.out.println("end of garbage collection");   
	}
	
	public void finalize() {
		System.out.println("Invocation of Finalize method");
	}

}
