package com.zert.java.tutorial.collections.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FinallyEx {

	public static void main(String[] args) {
		
		FileInputStream fis = null; 
		try {
			 fis = new FileInputStream("C:\\Users\\Ananya\\Desktop\\myfile.txt");
			int b ; 
			try {
				while((b=fis.read())!=-1) {
					System.out.print((char)b);
				}
			} catch (IOException e) {
				
				e.printStackTrace();
			}
							 
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} finally {
			 
				try {
					fis.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			 //finally block is used to close the resource code . it is a key tool to prevent the resource leaks. 
			}
		}
		
		
	}


