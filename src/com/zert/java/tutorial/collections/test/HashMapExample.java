package com.zert.java.tutorial.collections.test;

import java.util.HashMap;

/*
 
    It�s Unsorted & Unordered(no specific order (or) Sorting)
    In Map ,We can add values by put();
    Uniques of key is Maintain by overriding equals().
    Will allow NULL Key (or) Null Value
 */

public class HashMapExample {

      public static void main(String[] args) {

            HashMap<String, String> hasMap = new HashMap<String, String>();

            hasMap.put("Bahab", "Bad");

            hasMap.put("Arun", "BAD");

            hasMap.put("Zaheer", "G00D");

            hasMap.put("Sara", "G00D");

            hasMap.put("Xavier", "G00D");

            hasMap.put("Madan", null);

            hasMap.put(null, "Bad");

           

           System.out.println(hasMap.get("Arun"));

            System.out.println(hasMap);

           

            HashMap<Integer, String> hasMapNumber=new HashMap<Integer, String>();

            hasMapNumber.put(10, "G00D");

            hasMapNumber.put(9, "Bad");

            hasMapNumber.put(9, "Ok");

            hasMapNumber.put(8, "G00D");

            hasMapNumber.put(7, "Bad");

            hasMapNumber.put(null, "Bad");

            System.out.println(hasMapNumber);

      }

}
