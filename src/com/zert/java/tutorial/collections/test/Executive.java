package com.zert.java.tutorial.collections.test;

public class Executive extends Person {

	private int executiveId;
	private String designation;

	public Executive() {

	}

	public Executive(int executiveId, String designation, int age, String sex) {
		super(age, sex);
		this.executiveId = executiveId;
		this.designation = designation;
		

	}

	public int getExecutiveId() {
			 
		return executiveId;
	}

	public void setExecutiveId(int executiveId) {
		this.executiveId = executiveId;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

}
