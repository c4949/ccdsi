package com.zert.java.tutorial.collections.test;

import java.util.HashSet;
import java.util.Iterator;

/*
    It�s does�t maintain any order.
    It won�t allow duplicates.
    It�s Unsorted.
    It allow only one null value(because it wont allow duplicates).
 
 */

public class HashsetExample {

	public static void main(String[] args) {
		
		HashSet<Integer> hset = new HashSet<Integer>();
	
		hset.add(5); 
		hset.add(3);
		hset.add(2);
		hset.add(100);
		hset.add(100);
		hset.add(1000);
		hset.add(null);	
		hset.add(null);
		hset.add(null);
 		
		printIntegerHset(hset);	
		boolean isContains= hset.contains(100);
		System.out.println(isContains);
		
	
 
		
		HashSet<String> wordHashSet =  new HashSet<String>(); 
		wordHashSet.add("aadhu"); 
		wordHashSet.add("ani");
		
		printHashSet(wordHashSet);
		
		System.out.println("*****************************************");
		
		for (Integer number :hset) {
			System.out.println(number);
		}
		
		System.out.println("*****************************************");
		Iterator<Integer> iterator = hset.iterator();
		
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
 
		
		System.out.println("*****************************************");
		//Using forEach loop
		hset.forEach(number -> {
			System.out.println(number);
		});
		
		
		System.out.println("*****************************************");
		//Using forEach loop
		hset.forEach(System.out::println);
		
	}
	
	
	public static void printHashSet(HashSet<String> wordHashSet) {
		for(String word : wordHashSet) {
			System.out.println("The word is :" +word);
		}
		
		
		Iterator<String> iterator = wordHashSet.iterator();
		while(iterator.hasNext()) {
			System.out.println("The iterator value is : " + iterator.next());
		}
		
			
	}
	
	public static void printIntegerHset(HashSet<Integer> hset){
		for( Integer number: hset ) {
			System.out.println("The Number is"+number);
	
		}
		
		
		Iterator<Integer> iterator =  hset.iterator(); 
		while( iterator.hasNext() ) {
		System.out.println("Iterator value is" + iterator.next());	
		}
		
		hset.forEach(n->{
			System.out.println(n);
		});
		
			
		}
		
	}







