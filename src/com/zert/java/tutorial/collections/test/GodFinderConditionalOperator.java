package com.zert.java.tutorial.collections.test;

public class GodFinderConditionalOperator {

	public static void main(String[] args) {

		int age =11;
		
		if(age > 0) {
			
			if( isAKid(age) || isElderly(age)) {
				System.out.println("Generally, They are Gods so please be considerate with them");
			} else {
				System.out.println("Generally,  They sink in the mundane life with greediness, selfishness");
			}
			
			
			System.out.println("+++++++++++++++++++++++");
			System.out.println("+++++++++++++++++++++++");
			
			if( isAKid(age) | isElderly(age)) {
				System.out.println("Bitwise : Generally, They are Gods so please be considerate with them");
			} else {
				System.out.println("Bitwise : Generally,  They sink in the mundane life with greediness, selfishness");
			}
		} else {
			System.out.println("Please type a valid age");
		}
		
		
	}

	private static boolean isElderly(int age) {
		System.out.println("Is Elderly invoked");
		return age >=70;
	}

	private static boolean isAKid(int age) {
		System.out.println("isAKid invoked");
		return age >0 && age < 13;
	}

}
