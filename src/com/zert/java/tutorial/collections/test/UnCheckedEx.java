package com.zert.java.tutorial.collections.test;

public class UnCheckedEx {

	public static void main(String[] args) {

		// Unchecked exceptions are the exceptions which are not checked at the compile
		// time. The program does not throw an error while compiling the program. It is
		// mainly because bad input given to the program.
		// It is upto the programmer to check the input causing unchecked exception and
		// correct the same to avoid the unchecked exception.

		// Arithmetic exception .

		int a = 10;
		int b = 0;
		int result = a / b;
		System.out.println(result);

		// ArrayIndexOutOfBoundsException

		int array[] = { 1, 2, 3, 4, 5 };
		System.out.println(array[5]);

		String arr[] = { "ani", "aadhu" };
		System.out.println(arr[2]);

	}

}
