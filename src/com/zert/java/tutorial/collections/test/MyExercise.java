package com.zert.java.tutorial.collections.test;

public class MyExercise {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Address addressref  = new Address ("Gotha street","Fortitude valley","Perth"); 
		
		Address addressref1  = new Address ("Gotha street","Fortitude valley","California"); 
		
		/* addressref.setCity("Brisbane");
		addressref.setStreet("Gotha street");
		addressref.setSuburb("Fortitude valley");*/
		
		Employee employeeref = new Employee(100,"ananya",10000,25,"F",addressref); 
		
		System.out.println(employeeref);
		
		Employee employeeref1 = new Employee(101,"aadhavan",20000,18,"M",addressref1);
		
		System.out.println(employeeref1);
				
		
		Employee employeeref2 = new Employee();
		employeeref2.setEmpId(102);
		employeeref2.setEmpName("arun");
		employeeref2.setSalary(30000);
	    
		
		printemployee(employeeref); 
		printemployee(employeeref1); 
		printemployee(null);
		
		printemployee(employeeref1); 
		
		double bonusananya = calculateBonus(employeeref, 4.5);
				
		double bonusaadhavan = calculateBonus(employeeref1, 2.5);
		System.out.println(bonusananya);
		System.out.println(bonusaadhavan);
		
	}

	public static void printemployee(Employee employee) {
		
			if(employee != null) {
				System.out.println(employee);
				System.out.println(employee.getEmpName());
				System.out.println(employee.getSalary());
				
				if(employee.getAddress() != null) {
					Address ad = employee.getAddress();
					System.out.println(ad.getCity());
					System.out.println(ad.getSuburb());
					System.out.println(ad.getStreet());
				}
			
	}
			
		
		
	}
	
public static double calculateBonus(Employee employee, double interestpercent) { 
	
	double bonus =0;
	double salary = employee.getSalary();
	bonus= salary*interestpercent/100;
	return bonus;
		
	}
	
}
