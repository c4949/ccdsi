package com.zert.java.tutorial.collections.test;

public class RationChecker {
	
	
	public boolean checkRationCardEligibility(int age) {
		if (age >=18 && age<= 130) {
			return true;
		} else {
			if(age < 18) {
				throw new UnderAgeException("The person is a minor and not eligible for receiving Ration by his own. Veetla periyavanga iruntha vara sollu");
			} else if (age > 130) {
				throw new InvalidAgeException("Not a valid age. Please enter a realistic and valid age");
			}
		}
		return false;
	}

}
