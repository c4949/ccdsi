package com.zert.java.tutorial.collections.test;

public class Address {
	
	private String street;
	private String Suburb;
	private String city; 

	
	public Address() {
		
	}
	
public Address(String street, String Suburb, String city) {
	
	this.street= street;
	this.city=city; 
	this.Suburb=Suburb; 
		
	}
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getSuburb() {
		return Suburb;
	}
	public void setSuburb(String suburb) {
		Suburb = suburb;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	
	

}
