package com.zert.java.tutorial.collections.test;



class SuperA {
     void test() {
        System.out.println("Test in superclass method.. You will get surprise if you try overriding me :)");
    }
}

class SuperB extends SuperA {

      void  test() {
        System.out.println("test in subclass method");
    }
}

public class StaticMethodOverrideExample {
    public static void main(String arg[]) {

    //    SuperA myObject = new SuperA();
     //   myObject.test();
        // should be written as
    //    MyClass.test();
        // calling from subclass name
   //     MySubClass.test();
    	SuperA  myObject = new SuperB();
        myObject.test(); 
        
        
        // still calls the static method in MyClass, NOT in MySubClass
    }
}
