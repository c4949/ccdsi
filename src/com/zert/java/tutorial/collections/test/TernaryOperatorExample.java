package com.zert.java.tutorial.collections.test;

public class TernaryOperatorExample {

	public static void main(String[] args) {

		int a =5;
		int b=6;
		
		
		
		if(a>b) {
			System.out.println("A is Greater , normal if else ");
		} else {
			System.out.println("B is Greater , normal if else ");
		}
		
		
		

		String result = (a>b) ? "A is greater - tenary operator" : "B is greater - tenary operator";
		
		System.out.println(result);
				
	}

}
