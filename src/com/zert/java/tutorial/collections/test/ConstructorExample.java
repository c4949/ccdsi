package com.zert.java.tutorial.collections.test;

public class ConstructorExample extends SuperConstructorExample {

	int a;
	int b;
	int c;
	int d;
	int e;
	int f;
	int g;
	int h;
	int i;
	int j;
	int k;
	int l;
	
	public ConstructorExample () {
		
	}

	public ConstructorExample(int a, int b, int c, int d, int e, int f, int g, int h, int i, int j, int k, int l,int m, int n) {
		super(m,n); 
		this.a = a; // This refers the current object
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
		this.g = g;
		this.h = h;
		this.i = i;
		this.j = j;
		this.k = k;
		this.l = l;

	}

	public ConstructorExample(int a, int b, int c, int d,int m , int n) {
		super(m,n); // This is used to invoke  the super class constructor. suuper class constructor call should be the first statement in the subclass constructor
		
		super.m=56; // This is used to access the super class variable
		
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}

	public static void main(String[] args) {

		ConstructorExample constructorexample = new ConstructorExample(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13,14);
		
		System.out.println(constructorexample.a);
		System.out.println(constructorexample.b);

		ConstructorExample constructorexample1 = new ConstructorExample(11, 22, 33, 44,55,66);
				System.out.println(constructorexample1.a);
		System.out.println(constructorexample1.b);
		System.out.println(constructorexample1.c);
		System.out.println(constructorexample1.d);
		constructorexample1.e = 99;
		System.out.println(constructorexample1.e);
		System.out.println(constructorexample1.l);
		System.out.println(constructorexample.m);
		System.out.println(constructorexample.n);
		
		System.out.println(constructorexample1.m);
		System.out.println(constructorexample1.n);

		StringBuffer sb = new StringBuffer("aadhavan");
		
		ConstructorExample constructorexample3 = new ConstructorExample();
		constructorexample3.a=9;
		
	}
}
