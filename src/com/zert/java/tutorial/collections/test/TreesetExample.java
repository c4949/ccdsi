package com.zert.java.tutorial.collections.test;

import java.util.Iterator;
import java.util.TreeSet;

/*
     It won�t allow duplicates.
    It maintains ascending Order or chronological order
    It won�t allow null value.
  
 */

public class TreesetExample {

	public static void main(String[] args) {


		TreeSet<String> treeSet = new TreeSet<String>();
		treeSet.add("ananya");
		treeSet.add("aadhavan");
		treeSet.add("arun");
		//treeSet.add(null); It wont allow null values
		System.out.println(treeSet);

		for (String name : treeSet) {
			System.out.println(name);
		}
		
		
		 Iterator<String> it=treeSet.iterator();

         while(it.hasNext()){
               System.out.println(it.next());
         }
         
         
         TreeSet<Integer> treeSetForNumbers = new TreeSet<Integer>();

         treeSetForNumbers.add(98);

         treeSetForNumbers.add(23);

         treeSetForNumbers.add(65);

         treeSetForNumbers.add(89);

         treeSetForNumbers.add(100);

         treeSetForNumbers.add(10);

         treeSetForNumbers.add(44);

         treeSetForNumbers.add(132);

         System.out.println(treeSetForNumbers);

	}

}
