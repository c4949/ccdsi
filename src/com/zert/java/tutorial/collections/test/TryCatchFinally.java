package com.zert.java.tutorial.collections.test;

public class TryCatchFinally {

	  public static void main(String[] args) throws Exception {

	    try{
	        System.out.println('A');
	        try{
	            System.out.println('B');
	            throw new Exception("threw exception in B");
	        }
	        finally
	        {
	            System.out.println('X');
	        }
	      // Not reachable code System.out.println("This will not get executed");
	    }
	    catch(Exception e)
	    {
	        System.out.println('Y');
	    }
	    finally
	    {
	        System.out.println('Z');
	    }
	  }
	}