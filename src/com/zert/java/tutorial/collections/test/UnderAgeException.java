package com.zert.java.tutorial.collections.test;

public class UnderAgeException extends RuntimeException {
	 public UnderAgeException(String msg) {
		 super(msg);
	 }
}
