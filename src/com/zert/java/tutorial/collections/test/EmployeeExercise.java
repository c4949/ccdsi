package com.zert.java.tutorial.collections.test;

public class EmployeeExercise {

	public static void main(String[] args) {
		

 		Employee employee = new Employee(); 
 		Address address = new Address(); 
 		address.setCity("brisbane");
 		address.setStreet("Gotha st");
 		address.setSuburb("F.Valley");
		/*
		employee.setEmpId(100);
		employee.setEmpName("ananya");
		employee.setSalary(100000);
		*/
		
 		Employee employee1 = new Employee(100,"Ramu",5000.00,55, "M",address);
		
		
		System.out.println(employee1.getSex());
		
		
		
		Customer customer = new Customer(100, "Brisbane",50,"F"); 
		System.out.println(customer.getCustomerId());
		System.out.println(customer.getLocation());
		
		Executive executive = new Executive(2001,"Manager", 41, "Male"); 
		System.out.println(executive.getAge());
		System.out.println(executive.getSex());
		System.out.println(executive.getExecutiveId());
		System.out.println(executive.getDesignation());
	 
		
			
		
		System.out.println(employee1.getEmpId());
		System.out.println(employee1.getEmpName());
		System.out.println(employee1.getSalary());
		
		
		Employee employee2 = new Employee(101,"Ramya",500.00,76,"F",address);
		System.out.println(employee2.getEmpId());
		System.out.println(employee2.getEmpName());
		System.out.println(employee2.getSalary());
		
		Employee employee3 = new Employee(100,"Ramu");
		System.out.println(employee3.getEmpId());
		System.out.println(employee3.getEmpName());
		System.out.println(employee3.getSalary());
		
		
		/* employee.setEmpId(100);
		employee.setEmpName("aadhavan");
		employee.setSalary(100000);
*/
	}

}
