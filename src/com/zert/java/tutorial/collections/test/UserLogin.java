package com.zert.java.tutorial.collections.test;

public class UserLogin {
	
	static int loginCount = 0;
	
	
	public UserLogin() {
		loginCount++; // loginCount=loginCount+1
		
		System.out.println("The no of users logged in is:" + loginCount);
	
	}
	
	public static void main(String args[]) {
		UserLogin user1 = new UserLogin();
		UserLogin user2 = new UserLogin();
		UserLogin user3 = new UserLogin();
		UserLogin user4 = new UserLogin();
		UserLogin user5 = new UserLogin();
		UserLogin user6 = new UserLogin();
		UserLogin user7 = new UserLogin();
		UserLogin user8 = new UserLogin();
		UserLogin user9 = new UserLogin();
		UserLogin user10 = new UserLogin();
		UserLogin user11= new UserLogin();
		UserLogin user12= new UserLogin();
	}

}
