package com.zert.java.tutorial.collections.test;

import java.util.ArrayList;


/**
 * 
 * @author Ananya
 *  Note: An Array is not growable because you cant add values more than the declared length 
 *  Interview Question:
 *   An Arraylist is a growable array. You can insert any number of values 
 * 1. An arraylist will allow duplicate values to be added
 * 2. An arraylist maintains insertion order for both string and integer. 
 * 3. An arraylist can have any number of null values
 */

public class CollectionsExample {

	public static void main(String[] args) {
		
		ArrayList<String> names = new ArrayList<String>();
		names.add("Arun");
		names.add("Ram");
		names.add("Arun");
		
		names.add("John");
		names.add("Ravi");
		names.add("John");
		
		names.add(null);
		names.add(null);
		
		/*
		 * names.add(30);
		 * 
		 * names.add(30.0);
		 */
		
		
		
		for(int index=0; index<names.size(); index++) {
			System.out.println(names.get(index));
		}
		
		System.out.println("*********");
		// Enhanced for loop
		for(String name : names) {
			System.out.println(name);
		}
		
		System.out.println("*********");
		names.forEach(name -> {
			System.out.println(name);
		});
		
		
		

	}

}
