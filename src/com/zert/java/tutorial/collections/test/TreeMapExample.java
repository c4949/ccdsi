package com.zert.java.tutorial.collections.test;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


/*
It won�t allow duplicates.
It maintains ascending Order.
It won�t allow null value.
*/

public class TreeMapExample {
	
	public static void main (String[] args) {
		
		TreeMap<String, Integer> namemap = new TreeMap<String, Integer>(String.CASE_INSENSITIVE_ORDER);
		namemap.put("Zahir",35);
		namemap.put("aadhu",3);
		namemap.put("Rajmohan",72);
		namemap.put("jeeva",69);
		
		System.out.println(namemap);
		
		Integer j = namemap.get("jeeva"); 
		System.out.println(j);
		
		
		// using for each
		namemap.forEach((k, v) -> System.out.println((k + ":" + v)));
		
		// Using entry set
		
		for (Map.Entry<String, Integer> entry : namemap.entrySet()) {
	        System.out.println(entry.getKey() + ":" + entry.getValue());
	    }
		
		// Using key set
		Set<String> nameset = namemap.keySet();
		System.out.println(nameset);
		
		for ( String name : nameset) {
			System.out.println(name);
			Integer value = namemap.get(name);
			System.out.println(value);
		}
		
		// Using iterator of entry set
		
		 Iterator<Map.Entry<String, Integer>> iterator = namemap.entrySet().iterator();
		    while (iterator.hasNext()) {
		        Map.Entry<String, Integer> entry = iterator.next();
		        System.out.println(entry.getKey() + ":" + entry.getValue());
		    }
		
		
	}
	
}
