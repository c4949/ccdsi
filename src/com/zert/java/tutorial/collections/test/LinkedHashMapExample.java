package com.zert.java.tutorial.collections.test;

import java.util.LinkedHashMap;


/*
  It�s Insertion Order
  Fast iteration.
  Slower for insertion and deletion
  
*/

public class LinkedHashMapExample {

 

      public static void main(String[] args) {

            // TODO Auto-generated method stub

            LinkedHashMap<String, String> linkedHashMap=new LinkedHashMap<String, String>();

            linkedHashMap.put("Madan", "Bad");

            linkedHashMap.put("Wahab", "Bad");

            linkedHashMap.put("Arun", "BAD");

            linkedHashMap.put("Zaheer", "G00D");

            linkedHashMap.put(null, "G00D");

            linkedHashMap.put("Zaheer", null);

            linkedHashMap.put("Xavier", "G00D");

     

            System.out.println(linkedHashMap);

           

            LinkedHashMap<Integer, String> linkedHashMapNumber=new LinkedHashMap<Integer, String>();

            linkedHashMapNumber.put(10, "G00D");

            linkedHashMapNumber.put(new Integer(8), "Ok");

            linkedHashMapNumber.put(8, "G00D");

            linkedHashMapNumber.put(9, "Bad");

            linkedHashMapNumber.put(7, "Bad");

            linkedHashMapNumber.put(15, "Bad");

            linkedHashMapNumber.put(null, "Bad");

            linkedHashMapNumber.put(null, "Fine");

            System.out.println(linkedHashMapNumber);

 

      }

 

}