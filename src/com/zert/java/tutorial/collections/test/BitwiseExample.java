package com.zert.java.tutorial.collections.test;

public class BitwiseExample {

	public static void main(String[] args) {
		
		// Right shift
		int a = 8; // 1 0 0 0.
		int b = a >> 2; // 1 0 
		
		System.out.println(b);
		
		// Left Shift
		int c =8;  // 1 0 0 0.
		int d = c << 2; // 1 0 0 0 0 0
		
		System.out.println(d);
		
		int f = 12; //1 1 0 0
		int e = a & f; // 1 0 0 0
		
        //	  1 0 0 0
		//    1 1 0 0	
		//    ---------
		//    1 0 0 0 --> 8
		//    --------
		
		System.out.println("The Bitwise & , e :" + e);
		
		int g = a | f; // 1 1 0 0 
		
		  //  1 0 0 0
		  //  1 1 0 0	
		 //   ---------
		//    1 1 0 0 --> 12
		//    --------
		
		System.out.println("The bitwise or , g: " + g);
		
		
		int h = a ^ f; // 0 1 0 0 
		
	//	  1 0 0 0
	//    1 1 0 0	
	//    ---------
	//    0 1 0 0 --> 4
	//    --------
		// --> XOR if both are same the value is 0 else value is 1
		System.out.println("The value of XOR, h : " + h);
		
		int i = ~a; // Negation
		// The 32 bit representation with 1st digit as sign --> 0 - 0 0 0 .... 1 0 0 0 
		// -- Now the negation is  (-) on first bit and rest too is  negated -->  1  - 1 1 1 ..... 0 1 1 1  
		
		// 	Now to calculate 2's complement first need to calculate 1s complement 
		// -- 1 s complement needs to be calculated -- > 1 - 1 1 ..... 1 0 0 0 
		// -- add 1 to the end for 1's complement to get 2's complement --> 1 -  1 1 .... 1 0 0 1
		// Now the binary value of 1 0 0 1 is 9 and the first bit 1 represent (-)
		System.out.println(i);
		
		
	
		
		
	}

}
