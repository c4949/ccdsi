package com.zert.java.tutorial.collections.test;

public class SuperConstructorExample {

	int m; 
	int n; 
	
	public SuperConstructorExample() {
		
	}
	
	public SuperConstructorExample(int m, int n) {
		this.m= m; 
		this.n= n; 
	}
}
