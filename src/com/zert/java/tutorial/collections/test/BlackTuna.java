package com.zert.java.tutorial.collections.test;

public abstract  class BlackTuna extends Tuna {
	
	//if u uncomment this, it will throw an error because breathes method in tuna class is final and cannot be overridden. 

public void breathes() {
	System.out.println("tuna also breathes");
}
//
//	}
// the class extending BlackTuna class cannot override the swim method. as it is declared final. 
	public final void swim() {
		System.out.println("black tuna also swims");
	}
	
	

}
