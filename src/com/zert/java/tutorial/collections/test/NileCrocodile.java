package com.zert.java.tutorial.collections.test;

public class NileCrocodile extends SaltWaterCrocodile{

	public void feature() {
		System.out.println("Africa's largest crocodilian can reach a maximum size of about 20 feet and can weigh up to 1,650 pounds. Average sizes, though, are more in the range of 16 feet and 500 pounds. They live throughout sub-Saharan Africa, the Nile Basin, and Madagascar in rivers, freshwater marshes, and mangrove swamp");
	}
	
	public void swimsfast() {
		
		System.out.println("They can swim much faster by moving their bodies and tails in a sinuous fashion, and they can sustain this form of movement much longer than on land, with a maximum known swimming speed 30 to 35 km/h (19 to 22 mph), more than three times faster than any human.");
	
}
	
	public static void main (String args[]) {
	
		NileCrocodile nc = new NileCrocodile(); 
		nc.eats();
		nc.swimsfast();
		
		SaltWaterCrocodile swc = new SaltWaterCrocodile(); 
		swc.eats();
		swc.swimsfast();
	}

}