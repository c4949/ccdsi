package com.zert.java.tutorial.collections.test;


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TestHashMapExample {

	public static void main(String[] args) {
		
		
		HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
		hashMap.put(100, "Zaheer");	
		hashMap.put(99, "Xyno");	
		hashMap.put(95, "Raju");	
		hashMap.put(95, "Prasath");	
		hashMap.put(null, "xyz");	
	
		
		System.out.println(hashMap.get(100));
	
		
		Set<Map.Entry<Integer,String>> entrySet = hashMap.entrySet();
		
		for(Map.Entry<Integer,String> entry :entrySet) {
			System.out.println("The key is : " + entry.getKey() + ", The value is :" + entry.getValue());
			System.out.println(entry.getValue());
			System.out.println(entry.getKey());
		
		}
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		Set<Integer> keySet = hashMap.keySet();
		
		for(Integer empid : keySet ) {
			
			System.out.println("The key is : " + empid + ", The value is :" + hashMap.get(empid));
			
		}
		
		
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		
		LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<Integer, String>();
		linkedHashMap.put(100, "Zaheer");	
		linkedHashMap.put(99, "Xyno");	
		linkedHashMap.put(95, "Raju");	
		linkedHashMap.put(95, "Prasath");	
		linkedHashMap.put(null, "xyz");	
	
		
		System.out.println(linkedHashMap.get(100));
	
		
		Set<Map.Entry<Integer,String>> entrySetlinkedHashMap = linkedHashMap.entrySet();
		
		for(Map.Entry<Integer,String> entry :entrySetlinkedHashMap) {
			System.out.println("The key is : " + entry.getKey() + ", The value is :" + entry.getValue());
			System.out.println(entry.getValue());
			System.out.println(entry.getKey());
		
		}
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		Set<Integer> keySetentrySetlinkedHashMap = linkedHashMap.keySet();
		
		for(Integer empid : keySetentrySetlinkedHashMap ) {
			
			System.out.println("The key is : " + empid + ", The value is :" + linkedHashMap.get(empid));
			
		}
		
		
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		
		TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>();
		treeMap.put(100, "Zaheer");	
		treeMap.put(99, "Xyno");	
		treeMap.put(95, "Raju");	
		treeMap.put(95, "Prasath");	
		//treeMap.put(null, "xyz");	
	
		
		System.out.println(treeMap.get(100));
	
		
		Set<Map.Entry<Integer,String>> entrySetTreeHashMap = treeMap.entrySet();
		
		for(Map.Entry<Integer,String> entry :entrySetTreeHashMap) {
			System.out.println("The key is : " + entry.getKey() + ", The value is :" + entry.getValue());
			System.out.println(entry.getValue());
			System.out.println(entry.getKey());
		
		}
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$");
		Set<Integer> keySetentrySetTreeMap = treeMap.keySet();
		
		for(Integer empid : keySetentrySetTreeMap ) {
			
			System.out.println("The key is : " + empid + ", The value is :" + treeMap.get(empid));
			
		}
		
		
		
		

	}

}
