package com.zert.java.tutorial.collections.test;

public class QuadrarticEquation extends MathOperation{
	
	public int calculateEquation(int a,int b ) {
		int d=  super.sub (super.multiply(b,b), super.multiply(4,a));
			return d;
			
		
	}
	
	public static void main (String args[]) {
		QuadrarticEquation quadrarticequation = new QuadrarticEquation();
		int result = quadrarticequation.calculateEquation(10, 50);
		System.out.println(result);
		
	}
	
}
