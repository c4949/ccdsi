package com.zert.java.tutorial.collections.test;

public class UserDefinedExceptionExample {

	public static void main(String[] args) {
		RationChecker rationChecker = new RationChecker();
		boolean isEligible = rationChecker.checkRationCardEligibility(17);
		System.out.println("The person is eligible:" + isEligible);
		
	}

}
