package com.zert.java.tutorial.collections.test;

public class StringStringBuffer {

	public static void main(String[] args) {

		String str = "aadhavan";
		System.out.println(str.hashCode());

		str = str + "ananya";
		System.out.println(str.hashCode());

		StringBuffer sb = new StringBuffer("aadhavan");
		System.out.println(sb.hashCode());

		sb.append("ananya");
		System.out.println(sb.hashCode());

		String s1 = "aadhavan";
		concat1(s1);
		System.out.println(s1);

		StringBuilder s2 = new StringBuilder("aadhavan");
		concat2(s2);
		System.out.println(s2.toString());

		StringBuffer s3 = new StringBuffer("ananya");
		System.out.println(s3);
		concat3(s3);
		System.out.println(s3.toString());
		
		// string is immutable 
		// string buffer and string builder is mutable
		//string buffer is not thread safe whereas string builder is thread safe .
		
		
	}

	public static void concat1(String s1) {
		String s2 = s1.concat("arunprasath");
	}

	public static void concat2(StringBuilder s2) {
		s2.append("arunprasath");
	}

	public static void concat3(StringBuffer s3) {
		s3.append("arunprasath");
	}

}
