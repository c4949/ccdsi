package com.zert.java.tutorial.collections.test;

public class IncrementsAndDecrements {

	public static void main(String[] args) {
		
		int a=2;
		// The below is pre-increment and it increments in the same step
		System.out.println("Pre - Increment increments in the same step:" + ++a);
		
		int b=2;
		// The below is post  increment and it increments in the next step
		System.out.println("Post - Increment increments in the next step:" + b++);
		
		System.out.println("Post - Increment increment result is : "+ b);
		
		
		int c=8;
		System.out.println("Pre - Decrement decrement in the same step:" + --c);
		
		int d=8;
		System.out.println("Post- Decrement decrement in the same step:" + c--);
		System.out.println("Post - Decrement decrement result is:" +c);
		
	}

}
