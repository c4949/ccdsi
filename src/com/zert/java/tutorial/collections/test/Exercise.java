package com.zert.java.tutorial.collections.test;

public class Exercise {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		  Animal animalref = new Animal();
		  String result  = animalref.eats();
		  System.out.println(result);
		   
		  Calculate ref = new Calculate();
		//  ref.d=9f;//this variable cannot be accessed as it is a private variable. 
		  int value = ref.add(100,150); 
		  System.out.println(value);
		  
		  // MathOperation mathOperation = new MathOperation();
		  
		  
		  double arearesult = MathOperation.areaOfCircle(50.01); 
		  System.out.println(arearesult);
		  arearesult = MathOperation.areaOfCircle(30.01); 
		  System.out.println(arearesult);
		 
	// MathOperation.pi=4.14; // final variable value cannot be changed //  
		 System.out.println(MathOperation.pi);
		 System.out.println(MathOperation.multiply(7, 6));
		 System.out.println(MathOperation.sub(700, 800));
		 
		   
		 
	}

}
