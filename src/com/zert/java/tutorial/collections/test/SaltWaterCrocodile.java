package com.zert.java.tutorial.collections.test;

public class SaltWaterCrocodile implements Crocodile , Wildanimals{

	 
	public void eats() {
		System.out.println("Saltwater crocodile lives in saltwater and wetlands. The saltwater crocodile has a reputation for being one of the most aggressive species in the world as an adult");
		 	}

 
	public void swimsfast() {
	
		System.out.println("They are known to be fantastic swimmers and can travel long distances by sea, sometimes as much as 900km");
		
	}
	
	
}
