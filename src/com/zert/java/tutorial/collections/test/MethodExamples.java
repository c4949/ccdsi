package com.zert.java.tutorial.collections.test;

public class MethodExamples {

	// Method definition
	public static int add(int x, int y) {
		int c = x + y;
		return c;
	}

	public static int add(int x, int y, int z) {
		return x + y + z;
	}

	public static double add(double a, double b) {
		return a + b;
	}

	public int multiply() {
		int a = 5;
		int b = 7;
		return a * b;
	}

	public static void printRulesAndRegulations() {
		System.out.println("The customer cannot buy shares while they work with the organization");
		System.out.println("The customer cannot buy assets owned by the organization");
	}

	public static void invokeAdd(Department department) {
		System.out.println(department);
		/*
		 * System.out.println(department.getDepartmentId());
		 * System.out.println(department.getDepartmentName());
		 */
		int result = department.add(2, 5);
		System.out.println(result);
	}

	public static Department createDepartment() {
		Department department = new Department();
		return department;
	}

	public static void main(String[] args) {

		String s = "Test";
		int a = 8;
		int b = 9;
		int d = 12;
		int e = 9;

		MethodExamples methodExamples = new MethodExamples();

		int multiplicationResult = methodExamples.multiply();

		int z = MethodExamples.add(30, 50);

		int u = add(6, 5); // Method invocation

		int c = add(a, b);

		int n = add(d, e);

		System.out.println(u);

		System.out.println(c + d);

		int result = Math.addExact(c, n);

		System.out.println(result);

		printRulesAndRegulations();

		System.out.println(s);

		NumberComparison numberComparison = new NumberComparison();

		numberComparison.findBiggestOfThreeNumbers(8, 9, 10);

		numberComparison.findBiggestOfThreeNumbers(10, 9, 8);

		Department department = new Department();
		//department.empId =100;
		department.setEmpId(100);
//		System.out.println(department);

		/*
		 * department.setDepartmentId(100); department.setDepartmentName("mathematics");
		 */

		// System.out.println(department.getDepartmentId());
		// System.out.println(department.getDepartmentName());

		invokeAdd(department);
		
		Department d1 = createDepartment();
		d1.setEmpId(101);
		System.out.println(d1.getEmpId());
		

	}

}
