package com.zert.java.tutorial.collections.test;

public class Employee extends Person {

	private int empId;
	private String empName;

	private double salary;
	private Address address;

	public Employee() {
		System.out.println("employee constructor is invoked");
	}

	public Employee(int empId, String empName, double salary, int age, String sex, Address address) {

		// super();
		super(age, sex);
		this.empId = empId;
		this.empName = empName;
		this.salary = salary;
		this.address = address;

		super.sex = "M";
		super.getSex();

	}

	public Employee(int empId, String empName) {
		this.empId = empId;
		this.empName = empName;
	}

	public int getEmpId() {
		super.sex = "M";
		super.getSex();
		return empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

}
