package com.zert.java.tutorial.collections.test;

public class ExampleforString {

	public static void main(String[] args) {
		
		char c = 'A';
		
		int a =105;
		
		System.out.println(c);
		
		System.out.println((int)c);
		
		System.out.println((char) a);
		
		
		// The below code proves that String is immutable
		
		String s = "Arun";   // 100000
		System.out.println(s);
		
		System.out.println(s.concat("Prasath")); //200000
		
		System.out.println(s);
		
	//	System.out.println(s1);
		
		System.out.println("************************");
		
		// The below code proves that StringBuffer is mutable
		
		StringBuffer name = new StringBuffer("Arun"); // 50000
		System.out.println(name);
		
		name.append("Prasath");
		
		System.out.println(name);
		
		
		System.out.println("************************");
		
		StringBuilder nameBuilder = new StringBuilder("Arun");
		System.out.println(nameBuilder);
		nameBuilder.append("Prasath");
		System.out.println(nameBuilder);
		
		nameBuilder.reverse();
		System.out.println(nameBuilder);
		
		
		
		/*
		 * String s1 = "Arun";
		 * 
		 * if(s.equals(s1)) { System.out.println("Both are equal"); } else {
		 * System.out.println("Both are not equal"); }
		 * 
		 * String s2 = new String("Ramu");
		 * 
		 * String s3 = new String("Ramu");
		 * 
		 * 
		 * if(s2.equalsIgnoreCase(s3)) { System.out.println("Both are equal"); } else {
		 * System.out.println("Both are not equal"); }
		 */
		
		
		
		
		
		
		
		
		/*
		 concat()
		length()
		indexOf()
		lastIndexOf()
		split()
		substring()
		contains()
		trim()
		toLowerCase()
		toUpperCase()
		toCharArray()
		charAt()
		replace()
		compareTo()
		 
		 
		 
		 */

	}

}
