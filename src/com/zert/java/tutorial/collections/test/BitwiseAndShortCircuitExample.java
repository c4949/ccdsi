package com.zert.java.tutorial.collections.test;

public class BitwiseAndShortCircuitExample {

	public static void main(String[] args) {
		int a =10;
		int b= 6;
		int c= 12;
		
		if(a > b && a > c) { // &&  requires all the condition to be true to consider the expression true , but if first condition(a>b) is false fails it wont check  a> c
			System.out.println("A is the largest");
		} else if (b > a && b > c) {
			System.out.println("B is the largest");
		} else {
			System.out.println("C is largest");
		}
		
		
		if(a > b & a > c) { // &  requires all the condition to be true to consider the expression true , but if first condition(a>b) is false it will check  a> c which is a performance overhead
			System.out.println("A is the largest");
		} else if (b > a & b > c) {
			System.out.println("B is the largest");
		} else {
			System.out.println("C is largest");
		}
	}

}
