package com.zert.java.tutorial.collections.test;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 * @author Ananya
 *  Note: An Array is not growable because you cant add values more than the declared length 
 *  Interview Question:
 *   An Arraylist is a growable array. You can insert any number of values 
 * 1. An arraylist will allow duplicate values to be added
 * 2. An arraylist maintains insertion order for both string and integer. 
 * 3. An arraylist can have any number of null values
 */

public class ArrayListExample {

	
	
	public static void main(String[] args) {
		
		ArrayList<String> alist = new ArrayList<String>(); 
		
		alist.add("red");
		alist.add("blue");
		alist.add("Yellow");
		alist.add("Yellow");
		alist.add("green");
		
		alist.add(null);
		alist.add(null);
		alist.add(null);
	
		//alist.remove(0); 
		alist.add(2, "violet");
		
		ArrayList<String> alist1 = new ArrayList<String>(); 
		alist1.add("black");
		alist1.add("white");
	
		
		boolean status = alist.addAll(alist1);
		System.out.println(status);
		
        boolean ref = alist.contains("Yellow");
      if(ref==true) {
    	  System.out.println("colour is present");
      }else
      {
    	  System.out.println("colour is not present");
      }
		
      System.out.println("Used normal for loop below : Third method to loop through an arraylist");
      
      for (int index = 0; alist.size()-index>0; index++) {
			System.out.println(alist.get(index));
	  }
		
        System.out.println("Used Enhance loop below : Third method to loop through an arraylist");
		
		for(String colour:alist) {
			System.out.println(colour);
		}
		
		System.out.println("Use Iterator : Third method to loop through an array");
		
		Iterator<String> listIterator = alist.iterator();
				while(listIterator.hasNext()) {
			System.out.println(listIterator.next());
		
		}
		
	
 
		
		
		String[]  names = new String[4];
		
		names[0] = "Ramya";
		names[1] = "Rahul";
		names[2] = "Ajith";
		names[3] = "Shiva";

		
		for (int index = 0; names.length-index>0; index++) {
			System.out.println(names[index]);
		}
		
		
	
	}

}
