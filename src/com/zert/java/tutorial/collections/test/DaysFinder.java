package com.zert.java.tutorial.collections.test;

import java.util.Scanner;

public class DaysFinder {

	public static void main(String[] args) {

		try {

			System.out.println("Please enter the day of  a week in number:");
			Scanner scanner = new Scanner(System.in);
			Integer i = scanner.nextInt();

			/*
			 * if (i == 1) { System.out.println("sunday"); } else if (i == 2) {
			 * System.out.println("monday"); } else if (i == 3) {
			 * System.out.println("tuesday"); } else if (i == 4) {
			 * System.out.println("wednesday"); } else if (i == 5) {
			 * System.out.println("thursday ay"); } else if (i == 6) {
			 * System.out.println("friday"); } else if (i == 7) {
			 * System.out.println("saturday"); }else { System.out.println("Invalid day"); }
			 */

			// Integer i = Integer.parseInt(args[0]);

			switch (i) {
			case 1:
				System.out.println("Monday");
				break;
			case 2:
				System.out.println("Tuesday");
				break;
			case 3:
				System.out.println("Wednesday");
				break;
			case 4:
				System.out.println("Thursday");
				break;
			case 5:
				System.out.println("Friday");
				break;
			case 6:
				System.out.println("Saturday");
				break;
			case 7:
				System.out.println("Sunday");
				break;
			default:
				System.out.println("Not a valid day");
			}

		} catch (NumberFormatException e) {
			System.out.println("Please provide a valid number. The input you have entered is not a valid number");
		}
	}

}
