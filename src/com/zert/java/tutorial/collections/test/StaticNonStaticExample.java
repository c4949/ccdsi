package com.zert.java.tutorial.collections.test;

public class StaticNonStaticExample {

	static int a =0; 
	        int b = 0;
	        
	
	public static void main(String[] args) {
 
		StaticNonStaticExample ref = new StaticNonStaticExample();  
		ref.increment();
		    
		StaticNonStaticExample ref1 = new StaticNonStaticExample();  
		ref1.increment();
		
		StaticNonStaticExample ref2 = new StaticNonStaticExample();  
		ref2.increment();
	}
	
	public void increment() {
		
		
		
		a++;
	    b++;
	    
		int result1 = a;
		int result2 = b;
				
		
		System.out.println("value of a is "+ result1);
		System.out.println("value of b is "+result2);
		
	}

}
