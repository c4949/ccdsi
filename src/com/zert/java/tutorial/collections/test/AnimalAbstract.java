package com.zert.java.tutorial.collections.test;

public abstract class AnimalAbstract {
	
	public void eatsFood() {
		System.out.println("all animals eats food");
	}

	public abstract void sound(); 
}

       abstract class Dog extends AnimalAbstract {
    	  
    	  	public void eatsFood() {
    	  		System.out.println("dog eats food by biting");
    	  	}
    	  	public abstract void sniffs();   
    	  	
}
      
  class BritishBullDog extends Dog {
	
	public void sniffs() {
		System.out.println("Sniffs well and used by scotland yard");
	
	}

	public void sound() {
	  System.out.println("Barks well");
			
		}
		  
}
      