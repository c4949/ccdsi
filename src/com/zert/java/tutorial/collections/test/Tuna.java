package com.zert.java.tutorial.collections.test;

public  class Tuna extends Fish {
	
	private  final float PI=3.14f;
	
	public  static void main (String args[]) {
		Fish fish = new Fish();
		fish.swim();
		Fish tuna = new Tuna();
		tuna.swim();
		tuna.breathes();
	}

	    public  void swim() {
		System.out.println("tuna also swims");
	   	}
		

		public  void breathes() {
			System.out.println("tuna also breathes");
		
	}
	
	
}
