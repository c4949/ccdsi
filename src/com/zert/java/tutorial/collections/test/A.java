package com.zert.java.tutorial.collections.test;

public interface A {
	// An interface cannot have variables declared , you should intialize
	// int a;
	
	int a =7;
	
	public void test();
	
}
