package com.zert.java.tutorial.collections.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CheckedException {

	public static void main(String[] args) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C:\\Users\\Ananya\\Desktop\\myfile.txt");
			int a;
			while ((a = fis.read()) != -1) {
				System.out.println((char) a);
			}
	       // int b = 2 / 0;
	       // fis.close(); --> not a right place to close because if there is arithmetic exception the stream will not close and cause resource leakage. 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println(
				"The below 100000 line of code used to launch the rocket will not get impacted or stopped by exception becqause we handled it gracefully");

	}

}
