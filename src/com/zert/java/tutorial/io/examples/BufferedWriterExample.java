package com.zert.java.tutorial.io.examples;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedWriterExample {

	public static void main(String[] args) {

		File file = new File("C:\\Users\\Ananya\\Desktop\\AadhusCV.txt");
		BufferedWriter bufferedWriter = null;
			try {
				if(!file.exists()) {
					Boolean isFileCreated = file.createNewFile();
					System.out.println(isFileCreated);
				}
				
				FileWriter fileWriter = new FileWriter(file);
				
				 bufferedWriter = new BufferedWriter(fileWriter);
				bufferedWriter.newLine();
				bufferedWriter.write("Name: Aadhavan. He is Cute boy. He is a very kind baby.");
				bufferedWriter.newLine();
				bufferedWriter.write("Age: 3, Good Age to enjoy life");
				bufferedWriter.newLine();
				bufferedWriter.write("Name: Ananya. she is Cute girl. she is a very kind girl.");
				bufferedWriter.newLine();
				bufferedWriter.write("Age: 10, Good Age to shape life");
				
				int x =5/5;
				
			
				
				
				
				FileReader fileReader = new FileReader(file);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				
				String line = bufferedReader.readLine();
				
				while(line!=null) {
					System.out.println(line);
					line =  bufferedReader.readLine();
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println(" An error occured while writing to file");
				e.printStackTrace();
			} catch (IOException e) {
				
				System.out.println(" An error occured while writing to file");
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ArithmeticException e) {
				
				System.out.println(" An error occured while writing to file");
				//e.printStackTrace();
				
				
			}
			finally {
				try {
					bufferedWriter.flush();
					bufferedWriter.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
			
			
			
		
	}

}
